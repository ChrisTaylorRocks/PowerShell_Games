﻿#Pester test for the scripting games nov 2015

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = (Split-Path -Leaf $MyInvocation.MyCommand.Path) -replace '\.Tests\.', '.'
$Script = "$here\$sut"

Describe "Nov2015ScriptGames" {

    Context "When the String does not contain a ','" {
        $TestString = 'COMPUTER1'

        It "Returns the string given" {
            $ActualOutput = & $Script -VMNameStr $TestString

            $ActualOutput | Should Be $TestString
        }   
    }
    Context "When the string is ',' deliminated" {
       $TestString = 'COMPUTER1,COMPUTER2,, COMPUTER3 , COMPUTER 4, COMPUTER5'
       $ActualOutput = & $Script -VMNameStr $TestString
       It "Does not return null names" {
           $ActualOutput    | Should Not Be $null
       }
       It "Returns the individual VM Names without leading/trailing whitespace" {
           $ActualOutput.Count | Should Be 5
       
           $ActualOutput[0] | Should Be 'COMPUTER1'
           $ActualOutput[1] | Should Be 'COMPUTER2'
           $ActualOutput[2] | Should Be 'COMPUTER3'
           $ActualOutput[3] | Should Be 'COMPUTER 4'
           $ActualOutput[4] | Should Be 'COMPUTER5'
       }
    }

}