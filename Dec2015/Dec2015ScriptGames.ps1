﻿#Dec 2015 Scripting Games
cls
$list = @"
1 Partridge in a pear tree
2 Turtle Doves
3 French Hens
4 Calling Birds
5 Golden Rings
6 Geese a laying
7 Swans a swimming
8 Maids a milking
9 Ladies dancing
10 Lords a leaping
11 Pipers piping
12 Drummers drumming
"@

function Get-List{
    param(
        $list
    )
    $ListObejct = $list -split "[\r\n]" 
    foreach($item in $ListObejct){
        $pos = $item.IndexOf(" ")
        $object = New-Object –TypeName PSObject
        $object | Add-Member –MemberType NoteProperty –Name Counts –Value $item.Substring(0, $pos)
        $object | Add-Member –MemberType NoteProperty –Name Items –Value $item.Substring($pos+1)
        $object
    }    
}

Write-Output "-----------"
Write-Output "Sort by length:"
Get-List $list | Sort-Object {$_.Items.Length}
Write-Output "-----------"

Write-Output "-----------"
Write-Output "Length without number:"
(Get-List $list).items | Sort-Object {$_.Length}
Write-Output "-----------"

$Total = (Get-List $list).Counts | Measure-Object -Sum | select -ExpandProperty sum
Write-Output "-----------"
Write-Output "Total Items: $Total"
Write-Output "-----------"

$TotalBirds = $(Get-List $list | where {$_.items -in ('Partridge in a pear tree','Turtle Doves','French Hens','Calling Birds','Geese a laying','Swans a swimming')}).Counts | Measure-Object -Sum | select -ExpandProperty sum
Write-Output "-----------"
Write-Output "Total Birds: $TotalBirds"
Write-Output "-----------"

$Cumulative = 0
for ($i=1; $i -le $Total; $i++){
    (1..$i) | ForEach-Object {
        $Cumulative += $_
    }

}
Write-Output "-----------"
Write-Output "Cumulative Total: $Cumulative"
Write-Output "-----------"

